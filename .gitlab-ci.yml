# ---------------------------
# RHEL packaging workflow
# ---------------------------

stages:
  - Distribution
  - Source packages
  - Binary packages
  - Tests

include:
  # https://computing.docs.ligo.org/gitlab-ci-templates/
  - project: computing/gitlab-ci-templates
    # https://computing.docs.ligo.org/gitlab-ci-templates/rhel/
    file: rhel.yml

# -- macros

.el:
  variables:
    EPEL: "true"

.el7:
  image: igwn/base:el7-testing

.el8:
  image: igwn/base:el8-testing

.el9:
  image: igwn/base:el9-testing

.el7-build:
  extends: .el7
  image: igwn/builder:el7-testing

.el8-build:
  extends: .el8
  image: igwn/builder:el8-testing

.el9-build:
  extends: .el9
  # FIXME: igwn/builder:el9 isn't ready yet
  image: igwn/base:el9-testing

# -- distribution -----------
#
# Download the source distribution
#

dist:
  stage: Distribution
  image: igwn/builder:el8-testing
  before_script:
    - dnf -y install
        make
        rpmdevtools
  script:
    - make sources
  artifacts:
    paths:
      - "*.tar.*"

# -- source packages --------
#
# These jobs make src RPMs
#

.srpm:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/rhel/#.rhel:srpm
    - .rhel:srpm
  stage: Source packages
  needs:
    - dist
  variables:
    GIT_STRATEGY: fetch
  before_script:
    - !reference [".rhel:srpm", before_script]
    - ${DNF} -y install make rpmdevtools
  script:
    - make srpm

srpm:el7:
  extends:
    - .srpm
    - .el7-build

srpm:el8:
  extends:
    - .srpm
    - .el8-build

srpm:el9:
  extends:
    - .srpm
    - .el9-build

# -- binary packages --------
#
# These jobs generate binary RPMs
# from the src RPMs
#

.rpm:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/rhel/#.rhel:rpm
    - .rhel:rpm
  stage: Binary packages
  variables:
    SRPM: "*.src.rpm"
    # enable matlab
    RPMBUILD_OPTIONS: "--with matlab"

rpm:el7:
  extends:
    - .rpm
    - .el7-build
  needs:
    - srpm:el7

rpm:el8:
  extends:
    - .rpm
    - .el8-build
  needs:
    - srpm:el8

rpm:el9:
  extends:
    - .rpm
    - .el9-build
  needs:
    - srpm:el9

# -- test -------------------

.test:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/rhel/#.rhel:base
    - .rhel:base
  stage: Tests
  script:
    # install our packages
    - ${DNF} -y install *.rpm
    # sanity check
    - FrChannels
    - test -f /usr/share/framel/src/matlab/frgetvect.m

test:el7:
  extends:
    - .test
    - .el7
  needs:
    - rpm:el7

test:el8:
  extends:
    - .test
    - .el8
  needs:
    - rpm:el8
  script:
    - !reference [".test", script]
    - python3 -c "import framel"

test:el9:
  extends:
    - .test
    - .el9
  needs:
    - rpm:el9
  script:
    - !reference [".test", script]
    - python3 -c "import framel"
